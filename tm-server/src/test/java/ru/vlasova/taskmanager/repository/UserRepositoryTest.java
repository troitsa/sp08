package ru.vlasova.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ru.vlasova.taskmanager.model.entity.User;

import static org.junit.Assert.*;

public class UserRepositoryTest extends AbstractRepository {

    @Autowired
    @Qualifier(value = "userRepository")
    private UserRepository userRepository;

    @Test
    public void createUserTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        assertNotNull(userRepository.findById(user.getId()));
        userRepository.delete(user);
    }

    @Test
    public void findByUsername() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        Assert.assertEquals(userRepository.findByUsername(user.getUsername()), user);
        userRepository.delete(user);
    }

    @Test
    public void updateUserTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        Assert.assertEquals(userRepository.findById(user.getId()).orElse(null), user);
        user.setUsername("Tesstt");
        userRepository.save(user);
        Assert.assertEquals(userRepository.findById(user.getId()).orElse(null), user);
        userRepository.delete(user);
    }

    @Test
    public void removeUserTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        userRepository.delete(user);
        @Nullable final User user1 = userRepository.findById(user.getId()).orElse(null);
        assertNull(user1);
    }

}