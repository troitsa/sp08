package ru.vlasova.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ru.vlasova.taskmanager.model.entity.Project;
import ru.vlasova.taskmanager.model.entity.User;

import java.util.List;

import static org.junit.Assert.*;

public class ProjectRepositoryTest extends AbstractRepository {

    @Autowired
    @Qualifier(value = "userRepository")
    private UserRepository userRepository;

    @Autowired
    @Qualifier(value = "projectRepository")
    private ProjectRepository projectRepository;

    @Test
    public void createProjectTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        @NotNull final Project project1 = newProject("1");
        project1.setUser(user);
        projectRepository.save(project1);
        assertNotNull(projectRepository.findById(project1.getId()));
        projectRepository.delete(project1);
        userRepository.delete(user);
    }

    @Test
    public void findAllProjectByUserIdTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        @NotNull final Project project1 = newProject("1");
        project1.setUser(user);
        projectRepository.save(project1);
        assertTrue(projectRepository.findAllByUserId(user.getId()).contains(project1));
        projectRepository.delete(project1);
        userRepository.delete(user);
    }

    @Test
    public void findProjectTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        @NotNull final Project project1 = newProject("1");
        project1.setUser(user);
        projectRepository.save(project1);
        Assert.assertEquals(projectRepository.findById(project1.getId()).orElse(null), project1);
        projectRepository.delete(project1);
        userRepository.delete(user);
    }

    @Test
    public void findAllTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        @NotNull final Project project1 = newProject("1");
        @NotNull final Project project2 = newProject("2");
        project1.setUser(user);
        project2.setUser(user);
        projectRepository.save(project1);
        projectRepository.save(project2);
        List<Project> projects = (List<Project>) projectRepository.findAll();
        assertTrue(projects.size() > 0);
        assertTrue(projects.contains(project1) && projects.contains(project2));
        projectRepository.delete(project1);
        projectRepository.delete(project2);
        userRepository.delete(user);
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        @NotNull final Project project1 = newProject("1");
        @NotNull final Project project2 = newProject("2");
        project1.setUser(user);
        project2.setUser(user);
        projectRepository.save(project1);
        projectRepository.save(project2);
        List<Project> projects = projectRepository.findAllByUserId(user.getId());
        assertTrue(projects.size() == 2);
        assertTrue(projects.contains(project1) && projects.contains(project2));
        projectRepository.delete(project1);
        projectRepository.delete(project2);
        userRepository.delete(user);
    }

    @Test
    public void updateProjectTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        @NotNull final Project project1 = newProject("1");
        project1.setUser(user);
        projectRepository.save(project1);
        Assert.assertEquals(projectRepository.findById(project1.getId()).orElse(null), project1);
        project1.setName("Tesst");
        projectRepository.save(project1);
        Assert.assertEquals(projectRepository.findById(project1.getId()).orElse(null), project1);
        projectRepository.delete(project1);
        userRepository.delete(user);
    }

    @Test
    public void removeProjectByIdTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        @NotNull final Project project1 = newProject("1");
        project1.setUser(user);
        projectRepository.save(project1);
        Assert.assertEquals(projectRepository.findById(project1.getId()).orElse(null), project1);
        projectRepository.delete(project1);
        @Nullable final Project project2 = projectRepository.findById(project1.getId()).orElse(null);
        assertNull(project2);
        userRepository.delete(user);
    }

    @Test
    public void findAllByUserIdAndNameContainingIgnoreCaseOrDescriptionContainingIgnoreCaseTest() {
        @NotNull final User user = newUser("1");
        userRepository.save(user);
        @NotNull final Project project1 = newProject(" lloorreemm");
        @NotNull final Project project2 = newProject(" 2  lloorreemm");
        project1.setUser(user);
        project2.setUser(user);
        project1.setDescription(" lloorreemm 1");
        projectRepository.save(project1);
        projectRepository.save(project2);
        List<Project> projects = projectRepository
                .findAllByUserIdAndNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(user.getId(),
                        "lloorreemm",
                        "lloorreemm");
        assertTrue(projects.size() == 2);
        assertTrue(projects.contains(project1) && projects.contains(project2));
        projectRepository.delete(project1);
        projectRepository.delete(project2);
        userRepository.delete(user);
    }

}