package ru.vlasova.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vlasova.taskmanager.api.service.IProjectService;
import ru.vlasova.taskmanager.api.service.ITaskService;
import ru.vlasova.taskmanager.api.service.IUserService;
import ru.vlasova.taskmanager.model.entity.Project;
import ru.vlasova.taskmanager.model.entity.Task;
import ru.vlasova.taskmanager.model.entity.User;

import java.util.List;

import static org.junit.Assert.*;

public class TaskServiceTest extends AbstractService {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IProjectService projectService;

    @Test
    public void getTasksByProjectId() {
        @NotNull Project project = newProject("1");
        projectService.persist(project);
        @NotNull Task task1 = newTask("1");
        task1.setProject(project);
        taskService.persist(task1);
        @NotNull Task task2 = newTask("2");
        task2.setProject(project);
        taskService.persist(task2);
        List<Task> tasks = taskService.getTasksByProjectId(project.getId(), null);
        assertTrue(tasks.size() == 2);
        assertTrue(tasks.contains(task1) && tasks.contains(task2));
        taskService.remove(task1.getId());
        taskService.remove(task2.getId());
        projectService.remove(project.getId());
    }

    @Test
    public void remove() {
        @NotNull Task task = newTask("1");
        taskService.persist(task);
        taskService.remove(task.getId());
        @Nullable Task task1 = taskService.findOne(task.getId(), null);
        assertNull(task1);
    }

    @Test
    public void search() {
        @NotNull final Task task1 = newTask(" 1");
        @NotNull final Task task2 = newTask(" 2  lloorreemm");
        task1.setDescription(" lloorreemm 1");
        taskService.persist(task1);
        taskService.persist(task2);
        List<Task> tasks = taskService.search("lloorreemm", null);
        assertTrue(tasks.size() == 2);
        assertTrue(tasks.contains(task1) && tasks.contains(task2));
        taskService.remove(task1.getId());
        taskService.remove(task2.getId());
    }

    @Test
    public void merge() {
        @NotNull Task task = new Task();
        task.setName("testTask");
        taskService.persist(task);
        assertNotNull(taskService.findOne(task.getId(), null));
        taskService.remove(task.getId());
    }

    @Test
    public void persist() {
        @NotNull Task task = new Task();
        task.setName("testTask");
        taskService.persist(task);
        assertNotNull(taskService.findOne(task.getId(), null));
        taskService.remove(task.getId());
    }

    @Test
    public void findAll() {
        @NotNull final Task task1 = newTask("1");
        @NotNull final Task task2 = newTask("2");
        taskService.persist(task1);
        taskService.persist(task2);
        List<Task> tasks = (List<Task>) taskService.findAll();
        assertTrue(tasks.size() > 0);
        assertTrue(tasks.contains(task1) && tasks.contains(task2));
        taskService.remove(task1.getId());
        taskService.remove(task2.getId());
    }

    @Test
    public void findAllByUserId() {
        @NotNull final User user = newUser("1");
        userService.saveUser(user);
        @NotNull final Task task1 = newTask("1");
        @NotNull final Task task2 = newTask("2");
        task1.setUser(user);
        task2.setUser(user);
        taskService.persist(task1);
        taskService.persist(task2);
        List<Task> tasks = (List<Task>) taskService.findAllByUserId(user.getId());
        assertTrue(tasks.size() > 0);
        assertTrue(tasks.contains(task1) && tasks.contains(task2));
        taskService.remove(task1.getId());
        taskService.remove(task2.getId());
        userService.deleteUser(user.getId());
    }

    @Test
    public void findOne() {
        @NotNull final Task task1 = newTask("1");
        taskService.persist(task1);
        assertEquals(taskService.findOne(task1.getId(), null), task1);
        taskService.remove(task1.getId());
    }

    @Test
    public void sortTask() {
        @NotNull final Task task1 = newTask("1");
        @NotNull final Task task2 = newTask("2");
        @NotNull final Task task3 = newTask("3");
        task3.setName("ALorem");
        task2.setName("BLorem");
        task1.setName("CLorem");
        taskService.persist(task1);
        taskService.persist(task2);
        taskService.persist(task3);
        @NotNull final List<Task> sortTasks = taskService.sortTask("0", null);
        assertEquals("ALorem", sortTasks.get(0).getName());
        assertEquals("BLorem", sortTasks.get(1).getName());
        assertEquals("CLorem", sortTasks.get(2).getName());
        taskService.remove(task1.getId());
        taskService.remove(task2.getId());
        taskService.remove(task3.getId());
    }

}
