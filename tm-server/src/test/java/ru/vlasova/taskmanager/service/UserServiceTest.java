package ru.vlasova.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vlasova.taskmanager.api.service.IUserService;
import ru.vlasova.taskmanager.model.entity.User;

import java.util.List;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserServiceTest extends AbstractService {

    @Autowired
    private IUserService userService;

    @Test
    public void findUserById() {
        @NotNull final User user = newUser("1");
        userService.saveUser(user);
        assertEquals(userService.findUserById(user.getId()), user);
        userService.deleteUser(user.getId());
    }

    @Test
    public void allUsers() {
        @NotNull final User user = newUser("1");
        userService.saveUser(user);
        @NotNull final List<User> users = userService.allUsers();
        assertTrue(!users.isEmpty());
        assertTrue(users.contains(user));
        userService.deleteUser(user.getId());
    }

    @Test
    public void saveUser() {
        @NotNull final User user = newUser("1");
        userService.saveUser(user);
        assertEquals(userService.findByUsername(user.getUsername()), user);
        userService.deleteUser(user.getId());
    }

    @Test
    public void deleteUser() {
        @NotNull final User user = newUser("1");
        userService.saveUser(user);
        assertEquals(userService.findByUsername(user.getUsername()), user);
        userService.deleteUser(user.getId());
        @Nullable User user1 = userService.findUserById(user.getId());
        assertNull(user1);
    }

    @Test
    public void findByUsername() {
        @NotNull final User user = newUser("1");
        userService.saveUser(user);
        assertEquals(userService.findByUsername(user.getUsername()), user);
        userService.deleteUser(user.getId());
    }
}
