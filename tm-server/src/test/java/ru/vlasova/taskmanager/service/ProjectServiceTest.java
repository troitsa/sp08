package ru.vlasova.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vlasova.taskmanager.api.service.IProjectService;
import ru.vlasova.taskmanager.api.service.IUserService;
import ru.vlasova.taskmanager.model.entity.Project;
import ru.vlasova.taskmanager.model.entity.User;

import java.util.List;

import static org.junit.Assert.*;

public class ProjectServiceTest extends AbstractService {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IUserService userService;

    @Test
    public void remove() {
        @NotNull Project project = newProject("1");
        projectService.persist(project);
        projectService.remove(project.getId());
        @Nullable Project project1 = projectService.findOne(project.getId(), null);
        assertNull(project1);
    }

    @Test
    public void search() {
        @NotNull final Project project1 = newProject(" 1");
        @NotNull final Project project2 = newProject(" 2  lloorreemm");
        project1.setDescription(" lloorreemm 1");
        projectService.persist(project1);
        projectService.persist(project2);
        List<Project> projects = projectService.search("lloorreemm", null);
        assertTrue(projects.size() == 2);
        assertTrue(projects.contains(project1) && projects.contains(project2));
        projectService.remove(project1.getId());
        projectService.remove(project2.getId());
    }

    @Test
    public void merge() {
        @NotNull Project project = new Project();
        project.setName("testProject");
        projectService.persist(project);
        assertNotNull(projectService.findOne(project.getId(), null));
        projectService.remove(project.getId());
    }

    @Test
    public void persist() {
        @NotNull Project project = new Project();
        project.setName("testProject");
        projectService.persist(project);
        assertNotNull(projectService.findOne(project.getId(), null));
        projectService.remove(project.getId());
    }

    @Test
    public void findAll() {
        @NotNull final Project project1 = newProject("1");
        @NotNull final Project project2 = newProject("2");
        projectService.persist(project1);
        projectService.persist(project2);
        List<Project> projects = (List<Project>) projectService.findAll();
        assertTrue(projects.size() > 0);
        assertTrue(projects.contains(project1) && projects.contains(project2));
        projectService.remove(project1.getId());
        projectService.remove(project2.getId());
    }

    @Test
    public void findAllByUserId() {
        @NotNull final User user = newUser("1");
        userService.saveUser(user);
        @NotNull final Project project1 = newProject("1");
        @NotNull final Project project2 = newProject("2");
        project1.setUser(user);
        project2.setUser(user);
        projectService.persist(project1);
        projectService.persist(project2);
        List<Project> projects = (List<Project>) projectService.findAllByUserId(user.getId());
        assertTrue(projects.size() > 0);
        assertTrue(projects.contains(project1) && projects.contains(project2));
        projectService.remove(project1.getId());
        projectService.remove(project2.getId());
        userService.deleteUser(user.getId());
    }

    @Test
    public void findOne() {
        @NotNull final Project project1 = newProject("1");
        projectService.persist(project1);
        assertEquals(projectService.findOne(project1.getId(), null), project1);
        projectService.remove(project1.getId());
    }

    @Test
    public void sortProject() {
        @NotNull final Project project1 = newProject("1");
        @NotNull final Project project2 = newProject("2");
        @NotNull final Project project3 = newProject("3");
        project3.setName("ALorem");
        project2.setName("BLorem");
        project1.setName("CLorem");
        projectService.persist(project1);
        projectService.persist(project2);
        projectService.persist(project3);
        @NotNull final List<Project> sortProjects = projectService.sortProject("0", null);
        assertEquals("ALorem", sortProjects.get(0).getName());
        assertEquals("BLorem", sortProjects.get(1).getName());
        assertEquals("CLorem", sortProjects.get(2).getName());
        projectService.remove(project1.getId());
        projectService.remove(project2.getId());
        projectService.remove(project3.getId());
    }
}
