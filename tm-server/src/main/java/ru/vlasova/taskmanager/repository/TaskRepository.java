package ru.vlasova.taskmanager.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.vlasova.taskmanager.model.entity.Task;

import java.util.List;

@Repository
public interface TaskRepository extends CrudRepository<Task, String> {

    List<Task> findAllByProjectId(String projectId);

    List<Task> findAllByUserIdAndNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(String userId, String searchString1, String searchString2);

    List<Task> findAllByUserIdOrderByDateCreate(String userId);

    List<Task> findAllByUserIdOrderByDateStart(String userId);

    List<Task> findAllByUserIdOrderByDateFinish(String userId);

    List<Task> findAllByUserIdOrderByStatusAsc(String userId);

    List<Task> findAllByUserIdOrderByNameAsc(String userId);

    List<Task> findAllByUserId(String userId);

    Task findByIdAndUserId(String id, String userId);

}
