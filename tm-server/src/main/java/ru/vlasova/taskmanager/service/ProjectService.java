package ru.vlasova.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vlasova.taskmanager.api.service.IProjectService;
import ru.vlasova.taskmanager.api.service.IUserService;
import ru.vlasova.taskmanager.enumeration.Status;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;
import ru.vlasova.taskmanager.model.entity.Project;
import ru.vlasova.taskmanager.repository.ProjectRepository;

import java.util.Date;
import java.util.List;

@Service("projectService")
@Transactional
public class ProjectService implements IProjectService {

    @Autowired
    private IUserService userService;

    @Autowired
    private ProjectRepository repository;

    @Override
    @Nullable
    public Project insert(@Nullable final String name, @Nullable final String userId,
                          @Nullable final String description, @Nullable final Date dateStart,
                          @Nullable final Date dateFinish, @Nullable final String status) {
        final boolean checkGeneral = isValid(name, description);
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUser(userService.findUserById(userId));
        project.setDescription(description);
        project.setDateStart(dateStart);
        project.setDateFinish(dateFinish);
        project.setStatus(Status.valueOf(status));
        return project;
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        repository.deleteById(id);
    }

    @Override
    @Nullable
    public List<Project> search(@Nullable final String searchString, @Nullable final String userId) {
        if (searchString == null || searchString.trim().isEmpty()) return null;
        @NotNull final List<Project> projectList = repository.
                findAllByUserIdAndNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(userId, searchString, searchString);
        return projectList;
    }

    @Override
    public void merge(@Nullable final Project project) {
        if (project == null) return;
        repository.save(project);
    }

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) return;
        repository.save(project);
    }

    @Override
    @NotNull
    public List<Project> findAll() {
        @NotNull final List<Project> projectList = (List<Project>) repository.findAll();
        return projectList;
    }

    @Override
    @NotNull
    public List<Project> findAllByUserId(@Nullable final String userId) {
        @NotNull final List<Project> projectList = repository.findAllByUserId(userId);
        return projectList;
    }

    @Override
    @Nullable
    public Project findOne(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) return null;
        @Nullable final Project project = repository.findByIdAndUserId(id, userId);
        return project;
    }

    @Override
    public void removeAll() {
        repository.deleteAll();
    }

    @NotNull
    public List<Project> sortProject(@Nullable final String sortMode, @Nullable final String userId) {
        if (sortMode != null || !sortMode.isEmpty()) {
            switch (sortMode) {
                case ("1"):
                    return repository.findAllByUserIdOrderByDateCreate(userId);
                case ("2"):
                    return repository.findAllByUserIdOrderByDateStart(userId);
                case ("3"):
                    return repository.findAllByUserIdOrderByDateFinish(userId);
                case ("4"):
                    return repository.findAllByUserIdOrderByStatusAsc(userId);
            }
        }
        return repository.findAllByUserIdOrderByNameAsc(userId);
    }

    @Nullable
    public Project toProject(@Nullable final ProjectDTO projectDTO) {
        if (projectDTO == null) return null;
        @Nullable final Project project = new Project();
        project.setId(projectDTO.getId());
        project.setName(projectDTO.getName());
        project.setUser(userService.findUserById(projectDTO.getUserId()));
        project.setDescription(projectDTO.getDescription());
        project.setDateCreate(projectDTO.getDateCreate());
        project.setDateStart(projectDTO.getDateStart());
        project.setDateFinish(projectDTO.getDateFinish());
        project.setStatus(projectDTO.getStatus());
        return project;
    }

    @Nullable
    public ProjectDTO toProjectDTO(@Nullable final Project project) {
        if (project == null) return null;
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setUserId(project.getUser().getId());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setDateCreate(project.getDateCreate());
        projectDTO.setDateStart(project.getDateStart());
        projectDTO.setDateFinish(project.getDateFinish());
        projectDTO.setStatus(project.getStatus());
        return projectDTO;
    }

}