package ru.vlasova.taskmanager.enumeration;

import lombok.RequiredArgsConstructor;

public enum RoleType {
    USER,
    ADMIN;
}
