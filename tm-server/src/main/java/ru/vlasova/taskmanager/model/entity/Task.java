package ru.vlasova.taskmanager.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.taskmanager.enumeration.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Data
@Entity
@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Table(name = "app_task")
@JsonIgnoreProperties(value= {"user", "project"})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Task extends AbstractEntity {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    @ManyToOne
    private Project project;

    @Nullable
    @ManyToOne
    private User user;

    @Nullable
    @Column(nullable = false)
    private String name = "";

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String description = "";

    @NotNull
    @Column(updatable = false)
    private Date dateCreate = new Date(System.currentTimeMillis());

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Status status = Status.PLANNED;

    @Override
    @NotNull
    public String toString() {
        return "    Task " + name;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        @Nullable final Task task = (Task) o;
        if (name == null) return false;
        return name.equals(task.name);
    }
}
