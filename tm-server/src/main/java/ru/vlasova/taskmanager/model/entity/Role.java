package ru.vlasova.taskmanager.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import ru.vlasova.taskmanager.enumeration.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "app_role")
public class Role extends AbstractEntity implements GrantedAuthority {

    public Role(User user) {
        this.user = user;
    }

    @Id
    private String id = UUID.randomUUID().toString();

    @Enumerated(EnumType.STRING)
    private RoleType role = RoleType.USER;

    @ManyToOne
    private User user;

    @Override
    public String toString() {
        return role.toString();
    }

    @Override
    public String getAuthority() {
        return getRole().toString();
    }

}