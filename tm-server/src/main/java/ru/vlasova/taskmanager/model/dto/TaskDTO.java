package ru.vlasova.taskmanager.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskDTO extends ItemDTO {

    @Nullable
    private String projectId = "";

}
