package ru.vlasova.taskmanager.restcontroller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.vlasova.taskmanager.api.service.IUserService;
import ru.vlasova.taskmanager.model.dto.UserDTO;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserRestController {

    private final IUserService userService;

    @Autowired
    public UserRestController(IUserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/ruser")
    public ResponseEntity<?> createUser(@RequestBody UserDTO user) {
        userService.saveUser(userService.toUser(user));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/rusers")
    public ResponseEntity<List<UserDTO>> getUsers(@NotNull final String userId) {
        @NotNull final List<UserDTO> tasks = userService
                .allUsers()
                .stream()
                .map(userService::toUserDTO)
                .collect(Collectors.toList());
        return !tasks.isEmpty()
                ? new ResponseEntity<>(tasks, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping(value = "/ruser/{id}")
    public ResponseEntity<?> updateUser(@PathVariable(name = "id") String id,
                                        @RequestBody UserDTO user) {
        if (userService.findUserById(id) != null) {
            userService.saveUser(userService.toUser(user));
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @GetMapping(value = "/ruser/{id}")
    public ResponseEntity<UserDTO> getUser(@PathVariable(name = "id") String id) {
        @Nullable final UserDTO userDTO = userService.toUserDTO(userService.findUserById(id));
        return userDTO != null
                ? new ResponseEntity<>(userDTO, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "/ruser/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable(name = "id") String id) {
        userService.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/ruser/name/{name}")
    public ResponseEntity<UserDTO> getUserByName(@PathVariable(name = "name") String name) {
        final UserDTO userDTO = userService.toUserDTO(userService.findByUsername(name));
        return userDTO != null
                ? new ResponseEntity<>(userDTO, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
