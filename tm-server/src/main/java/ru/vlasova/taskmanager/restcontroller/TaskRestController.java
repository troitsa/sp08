package ru.vlasova.taskmanager.restcontroller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.vlasova.taskmanager.api.service.ITaskService;
import ru.vlasova.taskmanager.api.service.IUserService;
import ru.vlasova.taskmanager.model.dto.TaskDTO;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class TaskRestController {

    private final ITaskService taskService;

    @Autowired
    public TaskRestController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/rtask/{id}")
    public ResponseEntity<TaskDTO> getTask(@NotNull final String userId,
                                           @Nullable @PathVariable("id") final String id) {
        @Nullable final TaskDTO task = taskService.toTaskDTO(taskService.findOne(id, userId));
        return task != null
                ? new ResponseEntity<>(task, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/rtask")
    public ResponseEntity<?> createTask(@NotNull final String userId,
                                        @Nullable @RequestBody final TaskDTO task) {
        taskService.merge(taskService.toTask(task));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/rtasks")
    public ResponseEntity<List<TaskDTO>> getTasks(@NotNull final String userId) {
        @NotNull final List<TaskDTO> tasks = taskService
                .findAllByUserId(userId)
                .stream()
                .map(taskService::toTaskDTO)
                .collect(Collectors.toList());
        return !tasks.isEmpty()
                ? new ResponseEntity<>(tasks, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping(value = "/rtask/{id}")
    public ResponseEntity<?> updateTask(@NotNull final String userId,
                                        @Nullable @PathVariable(name = "id") final String id,
                                        @Nullable @RequestBody final TaskDTO task) {
        if (taskService.findOne(id, userId) != null) {
            taskService.merge(taskService.toTask(task));
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/rtask/{id}")
    public ResponseEntity<?> deleteTask(@NotNull final String userId,
                                        @Nullable @PathVariable(name = "id") final String id) {
        taskService.remove(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/rtasks/search/{keyword}")
    public ResponseEntity<List<TaskDTO>> searchTasks(@NotNull final String userId,
                                                     @Nullable @PathVariable(name = "keyword") final String keyword) {
        @NotNull final List<TaskDTO> tasks = taskService
                .search(keyword, userId)
                .stream()
                .map(taskService::toTaskDTO)
                .collect(Collectors.toList());
        return !tasks.isEmpty()
                ? new ResponseEntity<>(tasks, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/rtasks/byproject/{id}")
    public ResponseEntity<List<TaskDTO>> tasksByProject(@NotNull final String userId,
                                                        @Nullable @PathVariable(name = "id") final String id) {
        @NotNull final List<TaskDTO> tasks = taskService
                .getTasksByProjectId(id, userId)
                .stream()
                .map(taskService::toTaskDTO)
                .collect(Collectors.toList());
        return !tasks.isEmpty()
                ? new ResponseEntity<>(tasks, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/rproject/tasks/{id}")
    public ResponseEntity<List<TaskDTO>> getTasksByProject(@NotNull final String userId,
                                                           @Nullable @PathVariable("id") final String id) {
        @Nullable final List<TaskDTO> tasks = taskService
                .getTasksByProjectId(id, userId)
                .stream()
                .map(taskService::toTaskDTO)
                .collect(Collectors.toList());
        return !tasks.isEmpty()
                ? new ResponseEntity<>(tasks, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
