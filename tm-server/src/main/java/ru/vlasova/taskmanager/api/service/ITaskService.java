package ru.vlasova.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.taskmanager.model.dto.TaskDTO;
import ru.vlasova.taskmanager.model.entity.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IService<Task> {

    @Nullable
    Task insert(@Nullable final String name, @Nullable final String userId,
                @Nullable final String description, @Nullable final Date dateStart,
                @Nullable final Date dateFinish, @Nullable final String project,
                @Nullable final String status);

    @Nullable
    List<Task> getTasksByProjectId(@Nullable final String projectId, @Nullable final String userId);

    void remove(@Nullable final String id);

    @Nullable
    List<Task> search(@Nullable final String searchString, @Nullable final String userId);

    @Nullable
    List<Task> findAll();

    @Nullable
    Task findOne(@Nullable final String id, @Nullable final String userId);

    void persist(@Nullable final Task obj);

    void merge(@Nullable final Task obj);

    void removeAll();

    @Nullable
    List<Task> sortTask(@Nullable final String sortMode, @Nullable final String userId);

    @Nullable
    Task toTask(@Nullable final TaskDTO taskDTO);

    @Nullable
    TaskDTO toTaskDTO(@Nullable final Task task);

    @NotNull
    List<Task> findAllByUserId(@Nullable final String userId);
}
