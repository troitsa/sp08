package ru.vlasova.taskmanager.api.service;

import ru.vlasova.taskmanager.model.entity.AbstractEntity;

public interface IService<E extends AbstractEntity> {

    default boolean isValid(String... strings){
        for (String str : strings)
            if (str == null || str.isEmpty())
                return false;
        return true;
    }

}
