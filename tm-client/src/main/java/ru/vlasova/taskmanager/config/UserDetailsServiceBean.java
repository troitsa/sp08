package ru.vlasova.taskmanager.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.vlasova.taskmanager.client.Client;
import ru.vlasova.taskmanager.model.dto.UserDTO;
import ru.vlasova.taskmanager.model.enumeration.RoleType;

public class UserDetailsServiceBean implements UserDetailsService {

    @Autowired
    private Client client;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final UserDTO user = findByLogin(username);
        if (user == null) throw new UsernameNotFoundException("User not found.");
        org.springframework.security.core.userdetails.User.UserBuilder builder = null;
        builder = org.springframework.security.core.userdetails.User.withUsername(username);
        builder.password(user.getPassword());
        final RoleType userRole = user.getRole();
        builder.roles(userRole.toString());
        return builder.build();
    }
    private UserDTO findByLogin(String username) {
        return client.getUserByName(username);
    }
}
