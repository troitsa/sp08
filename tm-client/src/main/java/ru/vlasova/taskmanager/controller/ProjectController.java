package ru.vlasova.taskmanager.controller;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.vlasova.taskmanager.client.Client;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;
import ru.vlasova.taskmanager.model.dto.UserDTO;
import ru.vlasova.taskmanager.model.enumeration.Status;
import org.springframework.security.core.userdetails.User;
import java.util.Arrays;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class ProjectController {

    private final Client client;

    @RequestMapping(value = "/project_list")
    public ModelAndView projectList(@NotNull final Authentication authentication) {
        @Nullable final List<ProjectDTO> projectList = client
                .getProjects(getCurrentUsername(authentication).getId());
        @NotNull final ModelAndView mav = new ModelAndView("project_list");
        mav.addObject("projectList", projectList);
        return mav;
    }

    @RequestMapping(value = "/new_project")
    public String newProjectForm(@NotNull final Authentication authentication, ModelMap model) {
        @NotNull final ProjectDTO project = new ProjectDTO();
        @NotNull final UserDTO currentUser = getCurrentUsername(authentication);
        project.setUserId(currentUser.getId());
        model.addAttribute("project", project);
        @NotNull final List<Status> statusList = Arrays.asList(Status.values());
        model.addAttribute("statusList", statusList);
        return "new_project";
    }

    @RequestMapping(value = "project_save", method = RequestMethod.POST)
    public String saveProject(@NotNull final Authentication authentication,
                              @Nullable @ModelAttribute final ProjectDTO project) {
        project.setUserId(getCurrentUsername(authentication).getId());
        client.createProject(getCurrentUsername(authentication).getId(), project);
        return "redirect:/project_list";
    }

    @RequestMapping(value = "/edit_project")
    public ModelAndView editProjectForm(@NotNull final Authentication authentication,
                                        @NotNull @RequestParam final String id) {
        @NotNull final ModelAndView mav = new ModelAndView("edit_project");
        @Nullable final ProjectDTO project = client.getProject(getCurrentUsername(authentication).getId(), id);
        mav.addObject("project", project);
        @NotNull final List<Status> statusList = Arrays.asList(Status.values());
        mav.addObject("statusList", statusList);
        return mav;
    }

    @RequestMapping(value = "/delete_project")
    public String deleteProjectForm(@NotNull final Authentication authentication,
                                    @NotNull @RequestParam final String id) {
        client.deleteProject(getCurrentUsername(authentication).getId(), id);
        return "redirect:/project_list";
    }

    @RequestMapping(value = "search_project")
    public ModelAndView search(@NotNull final Authentication authentication,
                               @NotNull @RequestParam final String keyword) {
        @Nullable final List<ProjectDTO> result = client.searchProjects(getCurrentUsername(authentication).getId(), keyword);
        @NotNull final ModelAndView mav = new ModelAndView("search_project");
        mav.addObject("result", result);
        return mav;
    }

    private UserDTO getCurrentUsername(Authentication authentication) {
        User user = (User)authentication.getPrincipal();
        return client.getUserByName(user.getUsername());
    }

}
