package ru.vlasova.taskmanager.controller;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.vlasova.taskmanager.client.Client;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;
import ru.vlasova.taskmanager.model.dto.TaskDTO;
import ru.vlasova.taskmanager.model.dto.UserDTO;
import ru.vlasova.taskmanager.model.enumeration.Status;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class TaskController {

    private final Client client;

    @RequestMapping("/task_list")
    public ModelAndView taskList(@NotNull final Authentication authentication) {
        @Nullable final List<TaskDTO> taskList = client.getTasks(getCurrentUsername(authentication).getId());
        @NotNull final ModelAndView mav = new ModelAndView("task_list");
        mav.addObject("taskList", taskList);
        return mav;
    }

    @RequestMapping("/new_task")
    public String newTaskForm(@NotNull final Authentication authentication,
                              @NotNull final Map<String, Object> model) {
        @NotNull final TaskDTO task = new TaskDTO();
        @Nullable final List<ProjectDTO> projectList = client.getProjects(getCurrentUsername(authentication).getId());
        model.put("task", task);
        model.put("projectList", projectList);
        @NotNull final List<Status> statusList = Arrays.asList(Status.values());
        model.put("statusList", statusList);
        return "new_task";
    }

    @RequestMapping(value = "task_save", method = RequestMethod.POST)
    public String saveTask(@NotNull final Authentication authentication,
                           @Nullable @ModelAttribute final TaskDTO task) {
        task.setUserId(getCurrentUsername(authentication).getId());
        client.createTask(authentication.getName(), task);
        return "redirect:/task_list";
    }

    @RequestMapping("/edit_task")
    public ModelAndView editTaskForm(@NotNull final Authentication authentication,
                                     @NotNull @RequestParam final String id) {
        ModelAndView mav = new ModelAndView("edit_task");
        @Nullable final TaskDTO task = client.getTask(getCurrentUsername(authentication).getId(), id);
        @Nullable final List<ProjectDTO> projectList = client.getProjects(getCurrentUsername(authentication).getId());
        mav.addObject("task", task);
        mav.addObject("projectList", projectList);
        @NotNull final List<Status> statusList = Arrays.asList(Status.values());
        mav.addObject("statusList", statusList);
        return mav;
    }

    @RequestMapping("/delete_task")
    public String deleteTaskForm(@NotNull final Authentication authentication,
                                 @NotNull @RequestParam final String id) {
        client.deleteTask(getCurrentUsername(authentication).getId(), id);
        return "redirect:/task_list";
    }

    @RequestMapping("search_task")
    public ModelAndView search(@NotNull final Authentication authentication,
                               @NotNull @RequestParam final String keyword) {
        @Nullable final List<TaskDTO> result = client.searchTasks(getCurrentUsername(authentication).getId(), keyword);
        @NotNull final ModelAndView mav = new ModelAndView("search_task");
        mav.addObject("result", result);
        return mav;
    }

    private UserDTO getCurrentUsername(Authentication authentication) {
        User user = (User)authentication.getPrincipal();
        return client.getUserByName(user.getUsername());
    }
}
