package ru.vlasova.taskmanager.controller;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.vlasova.taskmanager.client.Client;
import ru.vlasova.taskmanager.model.dto.UserDTO;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class UserController {

    private final Client client;

    @GetMapping("registration")
    public String registration(@NotNull final Model model) {
        model.addAttribute("user", new UserDTO());
        return "registrate";
    }

    @PostMapping("registration")
    public String addUser(@NotNull @ModelAttribute("user") @Valid final UserDTO user,
                          @NotNull final BindingResult bindingResult,
                          @NotNull final Model model) {
        if (bindingResult.hasErrors()) {
            return "registrate";
        }
        client.createUser(user);
        return "redirect:login";
    }

    @GetMapping("/admin")
    @Secured({"ROLE_ADMIN"})
    public String userList(@NotNull final Model model) {
        model.addAttribute("allUsers", client.getUsers());
        return "admin";
    }

    @PostMapping("/admin/delete")
    @Secured({"ROLE_ADMIN"})
    public String deleteUser(@NotNull @RequestParam final String userId,
                             @NotNull @RequestParam final String action,
                             @NotNull final Model model) {
        if (action.equals("delete")) {
            client.deleteUser(userId);
        }
        return "redirect:/admin";
    }

    @GetMapping("/admin/get/{userId}")
    @Secured({"ROLE_ADMIN"})
    public String getUser(@NotNull @PathVariable("userId") final String userId,
                          @NotNull final Model model) {
        model.addAttribute("user", client.getUser(userId));
        return "admin";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("errorMsg", "Your username and password are invalid.");

        if (logout != null)
            model.addAttribute("msg", "You have been logged out successfully.");

        return "loginn";
    }
}
