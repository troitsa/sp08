package ru.vlasova.taskmanager.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;
import ru.vlasova.taskmanager.model.dto.TaskDTO;
import ru.vlasova.taskmanager.model.dto.UserDTO;

import java.util.List;

@FeignClient(value="tm-server", url="127.0.0.1:8081")
public interface Client {

    @PostMapping(value = "/rproject")
    ProjectDTO createProject(@RequestParam("userId") String userId, ProjectDTO project);

    @GetMapping(value = "/rprojects")
    List<ProjectDTO> getProjects(@RequestParam("userId") String userId);

    @GetMapping(value = "/rproject/{id}")
    ProjectDTO getProject(@RequestParam("userId") String userId,
                          @PathVariable("id") String id);

    @PutMapping(value = "/rproject/{id}")
    ProjectDTO updateProject(@RequestParam("userId") String userId,
                             @PathVariable("id") String id, ProjectDTO project);

    @DeleteMapping(value = "/rproject/{id}")
    void deleteProject(@RequestParam("userId") String userId, @PathVariable("id") String id);

    @GetMapping(value = "/rprojects/search/{keyword}")
    List<ProjectDTO> searchProjects(@RequestParam("userId") String userId,
                                    @PathVariable(name = "keyword") String keyword);

    @PostMapping(value = "/rtask")
    TaskDTO createTask(@RequestParam("userId") String userId,
                       TaskDTO task);

    @GetMapping(value = "/rtasks")
    List<TaskDTO> getTasks(@RequestParam("userId") String userId);

    @GetMapping(value = "/rtask/{id}")
    TaskDTO getTask(@RequestParam("userId") String userId,
                    @PathVariable("id") String id);

    @PutMapping(value = "/rtask/{id}")
    TaskDTO updateTask(@RequestHeader("Authorization") String header,
                       @PathVariable("id") String id, TaskDTO task);

    @DeleteMapping(value = "/rtask/{id}")
    void deleteTask(@RequestParam("userId") String userId,
                    @PathVariable("id") String id);

    @GetMapping(value = "/rtasks/search/{keyword}")
    List<TaskDTO> searchTasks(@RequestParam("userId") String userId,
                              @PathVariable(name = "keyword") String keyword);

    @GetMapping(value = "/rproject/tasks/{id}")
    List<TaskDTO> getTasksByProject(@RequestParam("userId") String userId,
                                    @PathVariable("id") String id);

    @PostMapping(value = "/ruser")
    UserDTO createUser(UserDTO user);

    @GetMapping(value = "/rusers")
    List<UserDTO> getUsers();

    @GetMapping(value = "/ruser/{id}")
    UserDTO getUser(@PathVariable(name = "id") String id);

    @DeleteMapping(value = "/ruser/{id}")
    void deleteUser(@PathVariable(name = "id") String id);

    @PutMapping(value = "/ruser/{id}")
    UserDTO updateUser(@PathVariable(name = "id") String id, UserDTO user);

    @GetMapping(value = "/ruser/name/{name}")
    UserDTO getUserByName(@PathVariable(name = "name") String name);

}
