package ru.vlasova.taskmanager.model.enumeration;

import lombok.RequiredArgsConstructor;

public enum RoleType {
    USER,
    ADMIN;
}
