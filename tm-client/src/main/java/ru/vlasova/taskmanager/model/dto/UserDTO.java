package ru.vlasova.taskmanager.model.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.taskmanager.model.enumeration.RoleType;

import java.util.UUID;

public class UserDTO {

    @Setter
    @Getter
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Setter
    @Getter
    @Nullable
    private String username;

    @Setter
    @Getter
    @Nullable
    private String password;

    @Setter
    @Getter
    @NotNull
    private RoleType role = RoleType.USER;

}
