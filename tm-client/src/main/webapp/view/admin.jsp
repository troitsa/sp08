<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="header.jsp" %>

<h1>Список пользователей</h1>

<table class="table table-striped">
    <col width="50px">
    <col>
    <col>
    <col width="20px">
    <col width="20px">
    <tr>
        <th>ID</th>
        <th>Login</th>
        <th>Edit</th>
        <th>Remove</th>
    </tr>
    <c:forEach items="${userList}" var="user">
        <tr>
            <td>${user.id}</td>
            <td>${user.username}</td>
            <td><a href="edit_user?id=${user.id}">Edit</a></td>
            <td><a href="delete_user?id=${user.id}">Delete</a></td>
        </tr>
    </c:forEach>
</table>

<%@ include file="footer.jsp" %>