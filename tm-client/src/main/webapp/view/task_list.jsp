<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="header.jsp" %>

<h1>Список задач</h1>

<div class="container-fluid">
    <div class="row justify-content-start">
        <div class="container-fluid row justify-content-between">
            <div class="col-6">
                <form method="get" action="search_task" class="form-inline">
                    <div class="form-group mx-sm-3 mb-2"><input type="text" id="keyword" name="keyword"/></div>
                    <button type="submit" class="btn btn-primary mb-2">OK</button>
                </form>
            </div>
            <div class="col-6">
                <a class="btn btn-primary" href="new_task" role="button">Создать новую задачу</a>
            </div>
        </div>
    </div>
</div>


<table class="table table-striped">
    <col width="50px">
    <col>
    <col>
    <col width="20px">
    <col width="20px">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Description</th>
        <th>Edit</th>
        <th>Remove</th>
    </tr>
    <c:forEach items="${taskList}" var="task">
        <tr>
            <td>${task.id}</td>
            <td>${task.name}</td>
            <td>${task.description}</td>
            <td><a href="edit_task?id=${task.id}">Edit</a></td>
            <td><a href="delete_task?id=${task.id}">Delete</a></td>
        </tr>
    </c:forEach>
</table>

<%@ include file="footer.jsp" %>