<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="utf-8" %>
<%@ include file="header.jsp" %>

<h1>Регистрация</h1>

<%--@elvariable id="user" type=""--%>
<form:form action="registration" method="post" modelAttribute="user" class="userForm">
    <div class="form-group">
        <label for="username">Логин:</label> <form:input path="username" name="username" class="form-control"/>
    </div>

    <div class="form-group">
        <label for="password">Пароль:</label> <form:input path="password" name="password" class="form-control"/>
    </div>

    <button class="btn btn-primary" type="submit">Зарегистрироваться</button>
</form:form>

<%@ include file="footer.jsp" %>
