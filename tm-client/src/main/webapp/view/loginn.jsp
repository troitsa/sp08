<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ include file="header.jsp" %>

<h1>Вход в систему</h1>

<sec:authorize access="isAuthenticated()">
    <% response.sendRedirect("/"); %>
</sec:authorize>

<form action="login" method="post">
    <div class="form-group">
        <label for="username">Login:</label> <input id="username" class="form-control" name="username"/>
    </div>
    <div class="form-group">
        <label for="password">Password:</label> <input id="password" class="form-control" name="password"/>
    </div>
    <button class="btn btn-primary" type="submit">Войти</button>
</form>

<h4><a href="registration">Зарегистрироваться</a></h4>

<%@ include file="footer.jsp" %>