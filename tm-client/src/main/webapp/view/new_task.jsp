<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="header.jsp" %>

<h1>Новая задача</h1>

<form:form action="task_save" method="post" modelAttribute="task" class="editForm">
    <div class="form-group">
        <label for="name">Название:</label> <form:input path="name" class="form-control"/>
    </div>
    <div class="form-group">
        <label for="description">Описание:</label> <form:input path="description" class="form-control"/>
    </div>
    <div class="form-group">
        <label for="dateStart">Дата начала:</label>
        <form:input id="dateStart" type="date" name="dateStart" path="dateStart" class="form-control"/>
    </div>
    <div class="form-group">
        <label for="dateFinish">Дата окончания:</label>
        <form:input id="dateFinish" type="date" name="dateFinish" path="dateFinish" class="form-control"/>
    </div>
    <div class="form-group">
        <c:if test="${not empty projectList}">
            <label for="project">Выберете проект</label>
            <form:select name="project" path="projectId" class="custom-select custom-select-lg mb-3">
                <form:option value="NONE" label=""/>
                <c:forEach var="listValue" items="${projectList}" varStatus="loop">
                    <form:option value="${listValue.id}">${listValue.name}</form:option>
                </c:forEach>
            </form:select>
        </c:if>
    </div>
    <div class="form-group">
        <c:if test="${not empty statusList}">
            <label for="status">Статус</label>
            <form:select name="status" path="status" class="custom-select custom-select-lg mb-3">
                <c:forEach var="listValue" items="${statusList}" varStatus="loop">
                    <form:option value="${listValue}">${listValue}</form:option>
                </c:forEach>
            </form:select>
        </c:if>
    </div>
    <button class="btn btn-primary" type="submit">OK</button>
</form:form>
<%@ include file="footer.jsp" %>